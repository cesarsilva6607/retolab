﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class Traffic
    {
        public static List<Commons.Model.Traffic> GetTraffic(DateTime dateStart, DateTime dateFinish, int? idVehicleType, int? skip, int? take)
        {
            var res = DataAccess.Traffic.GetTraffic(dateStart, dateFinish, idVehicleType, skip, take);
            return res;
        }

        public static Commons.Model.TraffiConsolidatedData GetConsolidatedData(string originCity, string destinationCity, string startDate, string finishDate)
        {
            var response = new Commons.Model.TraffiConsolidatedData();
            var res = DataAccess.Traffic.GetConsolidatedData(originCity, destinationCity, startDate, finishDate);

            response.data = res;

            //Total de registros
            response.records = res.Count;

            //Total de tipo de vehiculos
            response.typesVehicles = res.Select(x => x.vehicleType).Distinct().Count();

            //Ordenamiento de los viajes agrupados por fecha de cración
            var dateTraffic = res.GroupBy(x => x.createdOn.Value.Date).Select(g => new
            {
                date = g.Key,
                count = g.Count()
            }).OrderByDescending(x => x.count);

            //fecha con mayor cantidad de viajes
            response.dayWithHighestTraffic = dateTraffic.Select(x => x.date).FirstOrDefault().ToString("dd/MM/yyyy");
            
            //Cantidad de viajes del dia de mayor trafico
            response.countDayWithHighestTraffic = dateTraffic.Select(x => x.count).FirstOrDefault();

            //Cantidad de viajes promedio
            response.averageTripsPerDay = Convert.ToInt32(dateTraffic.Average(x => x.count));
            return response;
        }



    }
}
