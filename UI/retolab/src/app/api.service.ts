import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private httpClient: HttpClient) { }

  public getData(cityOrigin, cityDestiny, dateStart, dateFinish){
    return this.httpClient.get('http://qa.controlt.com.co/api_traficovehicular_retolab/GetConsolidatedData?startDate='+dateStart+'&finishDate='+dateFinish+'&originCity='+cityOrigin+'&destinationCity='+cityDestiny);
  }
}
