import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';

@Component({
    selector: 'dashboard-cmp',
    moduleId: module.id,
    templateUrl: 'dashboard.component.html'
})

export class DashboardComponent implements OnInit{

  ngOnInit() {
    
  }

  info = [];

  records: string;
  typesVehicles: string;
  dayWithHighestTraffic: string;
  countDayWithHighestTraffic: string;
  averageTripsPerDay: string;

  cityOrigin: string;
  cityDestiny: string;
  dateStart: string;
  dateFinish: string;
  
  constructor(private apiService: ApiService) { 
    this.records = '';
    this.typesVehicles = '';
    this.dayWithHighestTraffic = '';
    this.countDayWithHighestTraffic = '';
    this.averageTripsPerDay = '';
    this.info = [];
  }

    search(){

       this.apiService.getData(this.cityOrigin, this.cityDestiny, this.dateStart, this.dateFinish).subscribe((data : any)=>{
            this.records =  data.records;
            this.typesVehicles =  data.typesVehicles;
            this.dayWithHighestTraffic =  data.dayWithHighestTraffic;
            this.countDayWithHighestTraffic =  data.countDayWithHighestTraffic;
            this.averageTripsPerDay =  data.averageTripsPerDay;
            console.log(data.data);
            this.info = data.data;
        });
    }
}
