﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class Traffic
    {
        public static List<Commons.Model.Traffic> GetTraffic(DateTime dateStart, DateTime dateFinish, int? idVehicleType, int? skip, int? take)
        {

            var res = new List<Commons.Model.Traffic>();
            using (SqlConnection connection = new SqlConnection(Commons.Utilities.ConfigSettings.ConnectionStringApp))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                SqlTransaction transaction = null;
                transaction = connection.BeginTransaction("AppTransaction");
                command.Connection = connection;
                command.Transaction = transaction;
                command.CommandTimeout = int.MaxValue;
                try
                {
                    string conditionalIdVehicle = idVehicleType > 0 ? $"AND idVehicleTypology={idVehicleType} " : string.Empty;
                    string conditionalPagination = skip > 0 && take > 0 ? $"OFFSET {skip - 1} ROWS  FETCH NEXT {take} ROWS ONLY;" : string.Empty;

                    string query = $@"SELECT origin, destiny, createdOn, finishOn, vehicleType
                                    FROM CT.Report.Traffic
                                    WHERE createdOn BETWEEN '{dateStart:yyyy-MM-dd 00:00:00}' AND '{dateFinish:yyyy-MM-dd 23:59:59}' {conditionalIdVehicle}
                                    ORDER BY createdOn
                                    {conditionalPagination};";

                    command.CommandText = query;

                    using (IDataReader dataReader = command.ExecuteReader())
                    {
                        res = Commons.Utilities.MappingEntity.DataReaderMapToList<Commons.Model.Traffic>(dataReader);
                        dataReader.Close();
                    }
                }
                catch (SqlException ex)
                {

                }
                finally
                {
                    connection.Close();
                }
            }
            return res;
        }

        public static List<Commons.Model.Traffic> GetConsolidatedData(string originCity, string destinationCity, string startDate, string finishDate)
        {

            var res = new List<Commons.Model.Traffic>();
            using (SqlConnection connection = new SqlConnection(Commons.Utilities.ConfigSettings.ConnectionStringApp))
            {
                connection.Open();
                SqlCommand command = connection.CreateCommand();
                SqlTransaction transaction = null;
                transaction = connection.BeginTransaction("AppTransaction");
                command.Connection = connection;
                command.Transaction = transaction;
                command.CommandTimeout = int.MaxValue;
                try
                {
                    string conditional = !string.IsNullOrWhiteSpace(originCity) && !string.IsNullOrWhiteSpace(destinationCity) ? $" AND origin ='{originCity}' AND destiny ='{destinationCity}'" : string.Empty;


                    string query = $@"SELECT origin, destiny, createdOn, finishOn, vehicleType
                                    FROM CT.Report.Traffic
                                    WHERE createdOn BETWEEN '{startDate}' AND '{finishDate}' 
                                    {conditional}  ;";

                    command.CommandText = query;

                    using (IDataReader dataReader = command.ExecuteReader())
                    {
                        res = Commons.Utilities.MappingEntity.DataReaderMapToList<Commons.Model.Traffic>(dataReader);
                        dataReader.Close();
                    }
                }
                catch (SqlException ex)
                {

                }
                finally
                {
                    connection.Close();
                }
            }
            return res;
        }
    }
}
