﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication.ExtendedProtection;
using System.Text;

namespace Commons.Utilities
{
    public class VehicleType
    {
        public string GetNameVehicleType(int idType)
        {
            var listVehiclesType = new List<Model.VehicleType>();
            listVehiclesType.Add(new Model.VehicleType() { Id = 1, Name = "C2 Sencillo 10 ton de cap" });
            listVehiclesType.Add(new Model.VehicleType() { Id = 2, Name = "C2 X 0.6 (SUPER carry)" });
            listVehiclesType.Add(new Model.VehicleType() { Id = 3, Name = "C2 X 1 camioneta sencilla" });
            listVehiclesType.Add(new Model.VehicleType() { Id = 4, Name = "C2 X 1.5 Camioneta 4 x 4" });
            listVehiclesType.Add(new Model.VehicleType() { Id = 5, Name = "C2 X 2 Camioneta 2 ton de cap" });
            listVehiclesType.Add(new Model.VehicleType() { Id = 6, Name = "C2 X 3.5 turbo 3.5 ton de cap" });
            listVehiclesType.Add(new Model.VehicleType() { Id = 7, Name = "C2 X 4.5 turbo 4.5 ton de cap" });
            listVehiclesType.Add(new Model.VehicleType() { Id = 8, Name = "C2 X 5 turbo 5 ton de cap" });
            listVehiclesType.Add(new Model.VehicleType() { Id = 9, Name = "C2 X 7 turbo 7 ton de cap" });
            listVehiclesType.Add(new Model.VehicleType() { Id = 10, Name = "C2 X 7 Turbo 7 ton Extradimensionado" });
            listVehiclesType.Add(new Model.VehicleType() { Id = 11, Name = "C2 Sencillo 10 ton Extradimencionado" });
            listVehiclesType.Add(new Model.VehicleType() { Id = 12, Name = "C3 Doble troque" });
            listVehiclesType.Add(new Model.VehicleType() { Id = 13, Name = "C4 Rigido cuatro ejes" });
            listVehiclesType.Add(new Model.VehicleType() { Id = 14, Name = "Carro Tanque x 10 Ton de Cap" });
            listVehiclesType.Add(new Model.VehicleType() { Id = 15, Name = "Carro Tanque x 17 Ton de Cap" });
            listVehiclesType.Add(new Model.VehicleType() { Id = 16, Name = "Carro Tanque x 20 Ton de Cap" });
            listVehiclesType.Add(new Model.VehicleType() { Id = 17, Name = "Carro Tanque x 34 Ton de Cap" });
            listVehiclesType.Add(new Model.VehicleType() { Id = 18, Name = "Remolque de 2 ejes" });
            listVehiclesType.Add(new Model.VehicleType() { Id = 19, Name = "Remolque de 3 ejes" });
            listVehiclesType.Add(new Model.VehicleType() { Id = 20, Name = "Semiremolque de 1 eje" });
            listVehiclesType.Add(new Model.VehicleType() { Id = 21, Name = "Semiremolque de 2 ejes" });
            listVehiclesType.Add(new Model.VehicleType() { Id = 22, Name = "Semiremolque de 3 ejes" });
            listVehiclesType.Add(new Model.VehicleType() { Id = 23, Name = "BITREN" });
            listVehiclesType.Add(new Model.VehicleType() { Id = 24, Name = "Minimula" });
            listVehiclesType.Add(new Model.VehicleType() { Id = 25, Name = "Tractomula" });
            var result = listVehiclesType.Where(x => x.Id == idType).Select(v => v.Name).FirstOrDefault();
            return result;

        }
    }

}
