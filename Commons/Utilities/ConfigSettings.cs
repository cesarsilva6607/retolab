﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commons.Utilities
{
    public class ConfigSettings
    {
        /// <summary>
        /// Cadena de conexion para LINQ
        /// </summary>
        public static string ConnectionStringApp
        {
            get
            {
                string res = "";
                try
                {
                    res = System.Configuration.ConfigurationManager.ConnectionStrings["ConnectionString"].ToString();
                }
                catch (Exception ex)
                {

                }
                return res;
            }
        }
    }
}
