﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Commons.Model
{
    public class VehicleType
    {
        public string Name { get; set; }
        public int Id { get; set; }
    }
}
