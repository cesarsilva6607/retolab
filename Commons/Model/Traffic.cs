﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using System.Text;

namespace Commons.Model
{
    public class Traffic
    {
        //public string licencePlate { get; set; }
        public string origin { get; set; }
        public string destiny { get; set; }
        [Display(Name = "Fecha de creación")]
        [Required(ErrorMessage = "La fecha iniciar es requerida")]
        public DateTime? createdOn { get; set; }

        [Display(Name = "Fecha de finalización")]
        [Required(ErrorMessage = "La fecha final es requerida")]
        public DateTime? finishOn { get; set; }
        public string vehicleType { get; set; }
    }

    public class TraffiConsolidatedData
    {
        public int records { get; set; }
        public int typesVehicles { get; set; }
        public string dayWithHighestTraffic { get; set; }
        public int countDayWithHighestTraffic { get; set; }
        public int averageTripsPerDay { get; set; }
        public List<Traffic> data { get; set; }
    }
}
