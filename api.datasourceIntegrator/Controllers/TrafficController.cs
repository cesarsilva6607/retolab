﻿

using System;
using System.Collections.Generic;
using System.Web.Http;

namespace api.datasourceIntegrator.Controllers
{
    /// <summary>
    /// EndPoins para gestión de trafico
    /// </summary>
    public class TrafficController : ApiController
    {
        /// <summary>
        /// Obtener todos los datos de trafico
        /// </summary>
        /// <remarks>        
        /// Esta operación obtiene un objeto json con los datos de información de trafico.  
        /// Para la propiedad idVehicleType  estos son los código de tipologia del vehiculo:
        ///<br/>1 - C2 Sencillo 10 ton de cap,
        ///<br/>2 - C2 X 0.6 (SUPER carry),
        ///<br/>3 - C2 X 1 camioneta sencilla,
        ///<br/>4 - C2 X 1.5 Camioneta 4 x 4,
        ///<br/>5 - C2 X 2 Camioneta 2 ton de cap,
        ///<br/>6 - C2 X 3.5 turbo 3.5 ton de cap,
        ///<br/>7 - C2 X 4.5 turbo 4.5 ton de cap,
        ///<br/>8 - C2 X 5 turbo 5 ton de cap,
        ///<br/>9 - C2 X 7 turbo 7 ton de cap,
        ///<br/>10 - C2 X 7 Turbo 7 ton Extradimensionado,
        ///<br/>11 -C2 Sencillo 10 ton Extradimencionado,
        ///<br/>12 - C3 Doble troque,
        ///<br/>13 - C4 Rigido cuatro ejes,
        ///<br/>14 -Carro Tanque x 10 Ton de Cap,
        ///<br/>15 - Carro Tanque x 17 Ton de Cap,
        ///<br/>16 - Carro Tanque x 20 Ton de Cap,
        ///<br/>17 - Carro Tanque x 34 Ton de Cap,
        ///<br/>18 - Remolque de 2 ejes,
        ///<br/>19 - Remolque de 3 ejes,
        ///<br/>20 - Semiremolque de 1 eje,
        ///<br/>21 - Semiremolque de 2 ejes,
        ///<br/>22 - Semiremolque de 3 ejes,
        ///<br/>23 - BITREN,
        ///<br/>24 - Minimula,
        ///<br/>25 - Tractomula
        /// </remarks>
        /// <param name="startDate">Fecha inicial</param>
        /// <param name="finishDate">Fecha final</param>
        /// <param name="idVehicleType"> Identificador tipo vehiculo</param>
        /// <param name="page">Número de pagina </param>
        /// <param name="take">Cantidad de datos a obtener por pagina</param>        
        /// <returns></returns>
        [Route("GetAll")]
        public IHttpActionResult GetAll(string startDate, string finishDate, int? idVehicleType = null, int? page = null, int? take = null)
        {
            var respose = new List<Commons.Model.Traffic>();
            try
            {
                DateTime dtDateStart = Convert.ToDateTime(startDate);
                DateTime dtDateFinish = Convert.ToDateTime(finishDate);
                respose = Business.Traffic.GetTraffic(dtDateStart, dtDateFinish, Convert.ToInt32(idVehicleType), Convert.ToInt32(page), Convert.ToInt32(take));
            }
            catch (Exception)
            {
                //Registro de excepción
            }
            return Json(respose);
        }

        /// <summary>
        /// Recuperar el consolidado de información con sus respectivos datos
        /// </summary>
        /// <remarks>
        /// Esta operación obtiene toda la data asociada a los filtros ingresados y con propiedades que indican consolidados.
        /// </remarks>
        /// <param name="startDate"></param>
        /// <param name="finishDate"></param>
        /// <param name="originCity"></param>
        /// <param name="destinationCity"></param>
        /// <returns></returns>
        [Route("GetConsolidatedData")]
        public IHttpActionResult GetConsolidatedData(string startDate, string finishDate, string originCity = null, string destinationCity = null)
        {
            var respose = new Commons.Model.TraffiConsolidatedData();
            try
            {
                DateTime dtDateStart = Convert.ToDateTime(startDate);
                DateTime dtDateFinish = Convert.ToDateTime(finishDate);
                respose = Business.Traffic.GetConsolidatedData(originCity, destinationCity, startDate, finishDate);
            }
            catch (Exception)
            {
                //Registro de excepción
            }
            return Json(respose);
        }
    }
}
